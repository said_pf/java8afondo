package interfazComparable;

import java.util.Vector;

public class EjemploVector {

	public static void main(String [] args){
		Vector<String> vec = new Vector<>();
		
		vec.add("Said");
		vec.add("Rafa");
		vec.add("Xally");
		
		for(String var:vec){
			System.out.println(var);
		}
	}
}
