package interfazComparable;

public class TestOrdenar {

	public static void main(String[] args) {
		// definición y orden de array de alumnos
		Alumno arrA[] = {new Alumno("said", 31, 9.0),
						new Alumno("Xally", 24, 9.3),
						new Alumno("Rafael", 30, 9.4)};

		//ornedar
		Util.ordenar(arrA, new CriterioAlumnoNombre());
		Util.imprimir(arrA);
		
		Util.ordenar(arrA, new CirterioAlumnoNotaPromedio());
		Util.imprimir(arrA);
		/*
		//Definición y orden de array de strings
		String arrS[] = {"Said", "Aedro", "Pablo"};
		Util.ordenar(arrS);
		muestraArray(arrS);
		
		//DEfinición y orden de un array de Integers
		Integer arrI[] = {new Integer(4),
						new Integer(7),
						new Integer(0)};
		Util.ordenar(arrI);
		muestraArray(arrI);*/
	}
	
	public static void muestraArray(Comparable arr[]){
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}
	}

}
