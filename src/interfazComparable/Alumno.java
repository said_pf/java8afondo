package interfazComparable;

public class Alumno implements Comparable<Alumno>{

	private String nombre;
	private int edad;
	private Double notaPromedio;
	
	public Alumno(String nombre, int edad, Double promedio) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.notaPromedio = promedio;
	}

	@Override
	public int compareTo(Alumno otroAlumno) {
		return this.edad - otroAlumno.edad;
	}

	@Override
	public String toString() {
		return nombre+ " - "+edad+ " - " +notaPromedio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public Double getNotaPromedio() {
		return notaPromedio;
	}

	public void setNotaPromedio(Double notaPromedio) {
		this.notaPromedio = notaPromedio;
	}
	
}
