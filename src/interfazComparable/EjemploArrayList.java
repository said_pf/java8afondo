package interfazComparable;

import java.util.ArrayList;

public class EjemploArrayList {

	public static void main(String [] Args) {
		ArrayList<Integer> in = new ArrayList<>();
		
		in.add(new Integer(2));
		in.add(new Integer(6));
		in.add(new Integer(0));
		in.add(new Integer(3));
		
		for(int i:in){
			System.out.println(i);
		}
	}
}