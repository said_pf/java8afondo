package bases;

public class TrianguloAbstractHijo extends FiguraGeometricaAbstractPadre {

	private double base;
	private double altura;
	
	public TrianguloAbstractHijo(double base, double altura) {
		super("Triangulo");
		this.base = base;
		this.altura = altura;
	}

	@Override
	public double area() {
		return (base * altura)/2;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

}
