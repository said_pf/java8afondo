package bases;

public class CirculaAbstractHijo extends FiguraGeometricaAbstractPadre {

	private double radio;
	
	public CirculaAbstractHijo(double radio) {
		super("Circulo");
		this.radio = radio;
	}

	@Override
	public double area() {
		return Math.PI*Math.pow(radio, 2);
	}

	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}

}
