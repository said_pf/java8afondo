package bases;

public class PrincipalAbstract {

	public static void main(String[] args) {
	//polimorfismo + abstract
		RectanguloAbstract rec = new RectanguloAbstract(5, 10);
		CirculaAbstractHijo cir = new CirculaAbstractHijo(3.5);
		TrianguloAbstractHijo tri = new TrianguloAbstractHijo(5, 6);
		
		System.out.println(rec);
		System.out.println(cir);
		System.out.println(tri);
		
		FiguraGeometricaAbstractPadre arr[] = {
				new CirculaAbstractHijo(3.5),
				new RectanguloAbstract(5, 10),
				new TrianguloAbstractHijo(5, 6)};
		
		double prom = FiguraGeometricaAbstractPadre.areaPromedio(arr);
		System.out.println("El promedio de las areas de las figuras es: " + prom);
	}

}
