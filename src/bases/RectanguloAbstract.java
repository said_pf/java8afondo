package bases;

public class RectanguloAbstract extends FiguraGeometricaAbstractPadre {

	private double base;
	private double altura;
	
	public RectanguloAbstract(double base, double altura) {
		super("Rectangulo");
		this.base = base;
		this.altura = altura;
	}

	@Override
	public double area() {
		return base * altura;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

}
