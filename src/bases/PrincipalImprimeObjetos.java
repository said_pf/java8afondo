package bases;

import java.text.SimpleDateFormat;

public class PrincipalImprimeObjetos {

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("2/10/2020");
		
		Object[] arr = { sdf,
					new String("Esto es una cadena"),
					new Integer(34)
				};
		
		ImprimeObjetos.mostrar(arr);
	}

}
