package bases;

public abstract class FiguraGeometricaAbstractPadre {
	
	private String nombre;
	
	public FiguraGeometricaAbstractPadre(String nombre) {
		this.nombre = nombre;
	}

	public abstract double area();

	public static double areaPromedio(FiguraGeometricaAbstractPadre arr[]){
		int sum = 0;
		for(int i = 0; i < arr.length; i++){
			sum += arr[i].area();
		}
		return sum/arr.length;
	}
	
	public String toString() {
		return "Soy un " +nombre + " -> area = " + area();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
