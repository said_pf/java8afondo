package bases;

public class Principal {

	public static void main(String[] args) {
		//Modificación metodo toString objeto Fecha
		Fecha f1 = new Fecha(24,9,2020);	
		Fecha f2 = new Fecha(24,9,2020);
		
		System.out.println(f1);
		System.out.println(f2);
		
		if(f1.equals(f2)){
			System.out.println("Son iguales");
		} else{
			System.out.println("Son diferentes");
		}

	}

}
