
public class Principal {

	public static void main(String[] args) {
		try {
			Aplicacion ap = new Aplicacion();
			Usuario usr = ap.Login("juan", "juan");
			System.out.println(usr);
		} catch(Exception ex){
			System.out.println("Servicio temporalmente interrumpido: ");
			System.out.println(ex.getMessage());
		}
	}

}
