
public class Usuario {
	
	private String username;
	private String passWord;
	private String nombre;
	private String email;
	
	public Usuario() {
		super();
	}

	public Usuario(String userName, String passWord, String nombre, String email) {
		super();
		this.username = userName;
		this.passWord = passWord;
		this.nombre = nombre;
		this.email = email;
	}

	@Override
	public String toString() {
		return "Usuario [userName=" + username + ", passWord=" + passWord + ", nombre=" + nombre + ", email=" + email
				+ "]";
	}

	public String getUserName() {
		return username;
	}

	public void setUserName(String userName) {
		this.username = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
